# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.13](https://gitlab.com/wt0f/docker-terraform/-/compare/v1.6.6...v1.0.13) (2024-01-10)

### [1.0.12](https://gitlab.com/wt0f/docker-terraform/-/compare/v1.0.11...v1.0.12) (2024-01-10)

### 1.0.11 (2022-03-05)


### Bug Fixes

* Better description extraction ([37ca736](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/37ca736fc48199acece5237fbbe8375c1e94be93))
* Fixed extraction of version description ([ae24cbb](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/ae24cbbd74c667a164f2579140625589f42b88d0))
* Revised regex for description extraction ([60db2b0](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/60db2b04c2fc8cf3cd3b265a4f038edf3df956cf))


### Added

* a couple of perl test commands ([e9b6970](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/e9b69708701c0522f97762c99dea04b190e785eb))
* Added release GitLab CI jobs ([c6d31fd](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/c6d31fda379829506f057cbe7962af128a0cfe0e))
* Support for creating releases ([b0d00b6](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/b0d00b6e15f01c1da80c0392a30a81d12a203cb1))


### Changed

* Added quotes around description ([17418d9](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/17418d90344c363172e3f20292bac7123de00bad))
* Description extraction written to file ([6314caf](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/6314cafc4a9db869dfca54ba54020500f18edb3f))
* even more debugging ([c54f6ca](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/c54f6ca7e0a2f9fcf72fe0354f3f637c2407775b))
* exporting version var ([1261b18](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/1261b18e10289704f2aac57c7fcd49191f57d292))
* Modified how description is inserted into env file ([9e86f43](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/9e86f43940ffcd3446ec6c1b35dd75cbbfc30c2e))
* more debugging ([9a2e703](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/9a2e703888b888d88bd08f594d0889872418e6ef))
* More testing of description extraction ([e965ac1](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/e965ac1937b2afb7c8041a958a0d109559cbab1e))
* removed debugging code ([aec132c](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/aec132c51e41432a73980f59347a42062484c2aa))
* Set CI jobs not to run on tag pipelines ([153992c](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/153992ca0c63e3b61e2079d4c3f1620e294d4767))
* still debugging ([845f34f](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/845f34fbcdd356a8488962b58ca2499ca12ab603))
* Updated extraction of description ([5cdbfa4](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/5cdbfa4aa618f3be70b1868b6895b2fc5ba1286f))
* Updated how values communicate between jobs ([1ceb165](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/1ceb1652e2eaee1a50cb924b7fc1e9058e4d91fe))
* Updated the release description ([c891ae3](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/c891ae3c7ee86a4a76051cfd1a7faed214680f73))
* Using release-cli instead of release YAML ([ee49e14](https://wt0f.gitlab.com/wt0f/docker-terraform/-/commits/ee49e14160416a7237b1ee0cc3a682ecf3b9661b))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
