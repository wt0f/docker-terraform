# wt0f/docker-terraform

This is a forked copy of the https://github.com/broadinstitute/docker-terraform
project that has been customized for validating (i.e. syntax check) of
[Terraform](http://www.terraform.io/) code.

It is used in a number of GitLab CI workflows to perform a syntax check of
Terraform modules before use.
